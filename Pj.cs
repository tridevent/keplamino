﻿using UnityEngine;
using System.Collections;

public class Pj : MonoBehaviour {
    public float jump;// fuerza del salto
    private bool _floor;//detecta si esta tocando el suelo
    public float speed;// velocidad del pj
    private float _x;// movimiento en x
    private float _z;// movimiento en z
    private int _jumpCount;// cuenta los saltos
    public GameObject floor_fx;// sistema de particulas
	void Start () {
        _floor = true;// comenzamos tocando el suelo
	}
	
	
	void Update () {
        _x = Input.GetAxis("Horizontal");// asignar el eje horizontal A;D
        _z = Input.GetAxis("Vertical");// asignar el eje vertical W;S
        gameObject.transform.Translate(new Vector3(_x,0,_z)*speed*Time.deltaTime);//translate mover
        if (Input.GetKeyUp(KeyCode.K) && _floor==true)
            {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, jump, 0));//realiza el salto
            _jumpCount = _jumpCount + 1;
            }
        if (_jumpCount > 1)
        {
            if (Input.GetKeyUp(KeyCode.J))
            {
                gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, (-jump) *2, 0));//realiza el salto
                gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, -20, 0);
            }
            _floor = false;// no esta tocando el suelo 
        }
        if (Input.GetKey(KeyCode.Q) && _floor == true)
        {
            gameObject.transform.Rotate(new Vector3(0, -1, 0));//Rotate rotar 
        }
        if (Input.GetKey(KeyCode.E) && _floor == true)
        {
            gameObject.transform.Rotate(new Vector3(0, 1, 0));
        }
    }
    void OnCollisionEnter(Collision col)// si entra en una colision
    {
        if(_floor==false)
        {
            Instantiate(floor_fx,new Vector3(transform.position.x, transform.position.y- 0.5f, transform.position.z), floor_fx.transform.rotation);
        }
        _floor = true;//toca el suelo
        _jumpCount = 0;// reiniciamos la cantidad de saltos
    }
}
