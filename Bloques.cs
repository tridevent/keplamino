﻿using UnityEngine;
using System.Collections;

public class Bloques : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Debug.DrawRay(transform.position, Vector3.left*1000, Color.magenta);
        LanzarRayo(Input.mousePosition);// aqui pasamos la posicion del mouse cada frame por segundo mediante la funcion lanzarRayo
	}
    void LanzarRayo(Vector2 rayo)// Vector 2 = un valor en X y un valor en Y , rayo es el nombre de la variable
    {
        RaycastHit hit = new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(rayo);
        if (Physics.Raycast(ray, out hit, 100))
        {
            if(hit.collider.gameObject.tag=="bloqueInteractivo")
            {
                Debug.Log("bloque");
                if(Input.GetMouseButton(0))
                {
                    hit.collider.gameObject.transform.position = new Vector3(hit.transform.position.x, hit.point.y, hit.transform.position.z);
                }
            }
        }
    }
}
